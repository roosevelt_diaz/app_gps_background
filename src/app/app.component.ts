import { Component } from "@angular/core";

import { Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import {
  BackgroundGeolocation,
  BackgroundGeolocationConfig,
  BackgroundGeolocationResponse,
  BackgroundGeolocationEvents
} from "@ionic-native/background-geolocation/ngx";

declare var window;
@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"]
})
export class AppComponent {
  arr: any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private backgroundGeolocation: BackgroundGeolocation
  ) {
    this.arr = [];
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      const config: BackgroundGeolocationConfig = {
        desiredAccuracy: 10,
        stationaryRadius: 20,
        distanceFilter: 30,
        debug: false,
        stopOnTerminate: false,
        notificationTitle: "Prueba",
        notificationText: "Texto de prueba",
        notificationsEnabled: false,
        startForeground: false
      };

      console.log("start");

      this.backgroundGeolocation.configure(config).then(
        () => {
          this.backgroundGeolocation
            .on(BackgroundGeolocationEvents.location)
            .subscribe((location: BackgroundGeolocationResponse) => {
              var locationstr = localStorage.getItem("location");
              console.log("obtener ubicacion");
              if (locationstr == null) {
                this.arr.push(location);
              } else {
                var locationarr = JSON.parse(locationstr);
                this.arr = locationarr;
              }

              localStorage.setItem("location",JSON.stringify(this.arr));

            });
        } );
      window.app = this;
    });
  }
}
