import { Component } from "@angular/core";
import {
  BackgroundGeolocation,
  BackgroundGeolocationConfig,
  BackgroundGeolocationResponse,
  BackgroundGeolocationEvents
} from "@ionic-native/background-geolocation/ngx";

declare var window;
@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  logs: string[] = [];
  locations: any;
  constructor(private backgroundGeolocation: BackgroundGeolocation) {
    this.locations = [];
  }
  startBackground() {
    window.app.backgroundGeolocation.start();
  }

  stoptBackground() {
    window.app.backgroundGeolocation.stop();
  }

  getLocations() {
    console.log("get");
    console.log(localStorage.getItem("location"));
    this.locations =
      JSON.parse(localStorage.getItem("location")) == null
        ? []
        : JSON.parse(localStorage.getItem("location"));
  }

  clearLocations() {
    localStorage.removeItem("location");
  }
  
}
